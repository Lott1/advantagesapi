﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdvantageApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AdvantageApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly ApiContext _context;
        public OrdersController(ApiContext context)
        {
            _context = context;
        }


        [HttpGet("{pageIndex:int}/{pageSize:int}")]
        public IActionResult GetOrder(int pageIndex, int pageSize)
        {
            var data = _context.Orders.Include(o => o.customer).OrderBy(o => o.placed);

            var page = new PaginatedResponse<Order>(data, pageIndex, pageSize);

            var totalCount = data.Count();
            var totalPages = Math.Ceiling((double)totalCount / pageSize);

            var response = new
            {
                Page = page,
                TotalPages = totalPages
            };
            return Ok(response);
        }

        [HttpGet("ByState")]
        public IActionResult ByState()
        {
            var orders = _context.Orders.Include(o => o.customer).ToList();

            var groupedResult = orders.GroupBy(o => o.customer.state)
                .ToList()
                .Select(group => new
                {
                    State = group.Key,
                    Total = group.Sum(t => t.total)
                }).OrderByDescending(d => d.Total).ToList();

            return Ok(groupedResult);
        }

        [HttpGet("ByCustomer/{n}")]
        public IActionResult ByCustomer(int n)
        {
            var orders = _context.Orders.Include(o => o.customer).ToList();

            var groupedResult = orders.GroupBy(o => o.customer.id)
                .ToList()
                .Select(group => new
                {
                    Name = _context.Customers.Find(group.Key).name,
                    Total = group.Sum(t => t.total)
                }).OrderByDescending(d => d.Total)
                .Take(n)
                .ToList();

            return Ok(groupedResult);
        }

        [HttpGet("GetOrder/{id}", Name ="GetOrder")]
        public IActionResult GetOrder(int id)
        {
            var order = _context.Orders.Include(o => o.customer).First(o => o.id == id);
            return Ok(order);
        }
    }
}