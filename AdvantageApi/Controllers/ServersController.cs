﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdvantageApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdvantageApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServersController : ControllerBase
    {
        private readonly ApiContext _context;
        public ServersController(ApiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetServers()
        {
            var response = _context.Servers.OrderBy(s => s.id).ToList();

            return Ok(response);
        }

        [HttpGet("{id}",Name ="GetServer")]
        public IActionResult GetServer(int id)
        {
            var response = _context.Servers.Find(id);
            return Ok(response);
        }

        [HttpPut("{id}")]
        public IActionResult Message(int id, [FromBody] ServerMessage msg)
        {
            var server = _context.Servers.Find(id);

            if(server == null)
            {
                return NotFound();
            }


            if(msg.payload == "activate")
            {
                server.isOnline = true;
            }else if (msg.payload == "deactivate")
            {
                server.isOnline = false;
            }
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}