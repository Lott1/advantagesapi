﻿using AdvantageApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdvantageApi
{
    public class DataSeed
    {
        private readonly ApiContext _context;

        public DataSeed(ApiContext context)
        {
            _context = context;
        }

        public void SeedData(int nCustomers, int nOrders)
        {
            if (!_context.Customers.Any())
            {
                seedCustomers(nCustomers);
                _context.SaveChanges();

            }

            if (!_context.Orders.Any())
            {
                seedOrders(nOrders);
                _context.SaveChanges();
            }

            if (!_context.Servers.Any())
            {
                seedServers();
                _context.SaveChanges();

            }


        }
        private void seedServers()
        {
            List<Server> servers = buildServerList();

            foreach(var server in servers)
            {
                _context.Servers.Add(server);
            }
        }


        private void seedOrders(int n)
        {
            List<Order> orders = buildOrderList(n);

            foreach (var order in orders)
            {
                _context.Orders.Add(order);
            }
        }
        private void seedCustomers(int n)
        {
            List<Customer> customers = buildCustomerList(n);

            foreach(var customer in customers)
            {
                _context.Customers.Add(customer);
            }
        }

        private List<Customer> buildCustomerList(int nCustomers)
        {
            var customers = new List<Customer>();
            var names = new List<string>();
            for(var i = 1; i < nCustomers; i++)
            {
                var name = Helper.makeUniqueCustomerName(names);

                names.Add(name);

                customers.Add(new Customer
                {

                    id = i,
                    name = name,
                    email = Helper.makeCustomerEmail(name),
                    state = Helper.getRandomState()
                });
            }

            return customers;
        }
        private List<Order> buildOrderList(int nOrders)
        {
            var orders = new List<Order>();
            var rand = new Random();



            for (var i = 1; i < nOrders; i++)
            {
                var placed = Helper.getRandomOrderPlaced();
                var completed = Helper.getRandomOrderCompleted(placed);
                var randCustomerId = rand.Next(1, _context.Customers.Count());
                var customers = _context.Customers.ToList();
                
                orders.Add(
                    new Order
                    {
                        id = i,
                        customer = customers.First(c => c.id == randCustomerId),
                        total = Helper.getRandomOrderTotal(),
                        placed = placed,
                        completed = completed
                    }
                    );
            }

            return orders;
        }
        private List<Server> buildServerList()
        {
            return new List<Server>()
            {
                new Server
                {
                    id=1,
                    name="Dev-Web",
                    isOnline=true
                },
                new Server
                {
                    id=2,
                    name="Dev-Mail",
                    isOnline=true
                },
                new Server
                {
                    id=3,
                    name="Dev-Services",
                    isOnline=false
                },
                new Server
                {
                    id=4,
                    name="QA-Web",
                    isOnline=true
                },
                new Server
                {
                    id=5,
                    name="QA-Mail",
                    isOnline=false
                },
                new Server
                {
                    id=6,
                    name="QA-Services",
                    isOnline=true
                },
                 new Server
                {
                    id=7,
                    name="Prod-Web",
                    isOnline=true
                },
                new Server
                {
                    id=8,
                    name="Prod-Mail",
                    isOnline=true
                },
                new Server
                {
                    id=9,
                    name="Prod-Services",
                    isOnline=true
                }
};
            
        }
    }
}
