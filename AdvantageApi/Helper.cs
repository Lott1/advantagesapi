﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdvantageApi
{
    public class Helper
    {
        private static Random _rand = new Random();

        private static string getRandom(IList<string> items)
        {
            return items[_rand.Next(items.Count)];
        }

        internal static string makeUniqueCustomerName(List<string> names)
        {

            var maxNames = bizPrefix.Count * bizSuffix.Count;
            if(names.Count >= maxNames)
            {
                throw new System.InvalidOperationException("Maximum number of unique names exceeded");
            }
            var prefix = getRandom(bizPrefix);
            var suffix = getRandom(bizSuffix);
            var fullName = prefix + suffix;
            if (names.Contains(fullName))
            {
                makeUniqueCustomerName(names);
            }
            return fullName;
        }

        private static readonly List<string> bizPrefix = new List<string>()
        {
            "MainSt",
            "Enterprise",
            "Ready",
            "Quick",
            "Budget",
            "Peak",
            "Magic",
            "Family",
            "Comfort"
        };

        private static readonly List<string> bizSuffix = new List<string>()
        {
            "Corporation",
            "Co",
            "Logistics",
            "Transit",
            "Bakery",
            "Goods",
            "Foods",
            "Cleaners",
            "Hotels",
            "Planners",
            "Automotive",
            "Books",
        };

        internal static string makeCustomerEmail(string customerName)
        {
            return $"contact@{customerName.ToLower()}.com";
        }

        internal static string getRandomState()
        {
            return getRandom(usStates);
        }

        private static readonly List<string> usStates = new List<string>()
        {
            "AK", "AL", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA",
            "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
            "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ"
        };

        internal static decimal getRandomOrderTotal()
        {
            return _rand.Next(100, 5000);
        }

        internal static DateTime getRandomOrderPlaced()
        {
            var end = DateTime.Now;
            var start = end.AddDays(-90);
            TimeSpan possibleTimeSpan = end - start;
            TimeSpan newTimeSpan = new TimeSpan(0, _rand.Next(0, (int)possibleTimeSpan.TotalMinutes),0);
            return start + newTimeSpan;

        }
        internal static DateTime? getRandomOrderCompleted(DateTime orderPlaced)
        {

            var now = DateTime.Now;
            var minLeadTime = TimeSpan.FromDays(7);
            var timePassed = now - orderPlaced;

            if (timePassed < minLeadTime)
            {
                return null;
            }

            return orderPlaced.AddDays(_rand.Next(7, 14));

        }
    }

}
