﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdvantageApi.Models
{
    public class ServerMessage
    {

        public int id { get; set; }
        public string payload { get; set; }
    }
}
