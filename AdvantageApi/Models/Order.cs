﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdvantageApi.Models
{
    public class Order
    {
        public int id { get; set; }
        public Customer customer { get; set; }
        public decimal total { get; set; }
        public DateTime placed { get; set; }
        public DateTime? completed { get; set; }

    }
}
