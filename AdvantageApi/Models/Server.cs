﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdvantageApi.Models
{
    public class Server
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool isOnline { get; set; }

    }
}
